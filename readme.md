# Nettverk

A C# simplified socket for one to one sending of strings as packets.

## Getting Started

Nettverk is intended to be compiled as a DLL for use in other projects. The applications of it are ideal for one on one packet sending, such as file transfers or chat messages.
There is an example Test Chat application to see how it works, as well as the open source applications of it for file transfer.

### Prerequisites

The library/application requires .NET 4.5

### Installing

Compile the application through VS or a compiler of your preffered choice annd use the DLL as a reference to your own projects.

If changes to the Nettverk code is desired, add the project folder to a solution in VS and then use as a reference.

## Authors

* **Daniel Stamer-Squair** - *Uaine Teine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details