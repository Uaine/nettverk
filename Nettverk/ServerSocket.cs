﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Nettverk
{
    public class ServerSocket
    {
        NettverkSocket lookingformoreserver = new NettverkSocket(true);
        List<NettverkSocket> clientServers = new List<NettverkSocket>();
        public int noClients = 0;
        bool NotConnectedIP(string ip)
        {
            bool none = true;
            for (int i = 0; i < noClients; i++)
            {
                if (clientServers[i].connectedIP == ip)
                {
                    none = false;
                }
            }
            return none;
        }
        public void OpenServer(int portToAlwaysListen, int porttoconnecttoafter)
        {
            while (true)
            {
                //lookingformoreserver = new NettverkSocket(true);
                lookingformoreserver.RunServer("any", portToAlwaysListen);
                string newip = lookingformoreserver.connectedIP;
                if (NotConnectedIP(newip) == true)
                {
                    clientServers.Add(new NettverkSocket(true));
                    clientServers[noClients].RunServer(newip, porttoconnecttoafter);
                    noClients += 1; //We have a new client now
                }
                Thread.Sleep(100);
            }
        }
        public bool IsSending()
        {
            if (noClients == 0)
                return false;
            else
                return clientServers[0].IsSending();
        }
        public int getmsgtype(string msg)
        {
            string strype = msg.Remove(2, msg.Length - 2);
            return Convert.ToInt32(strype);
        }
        public void Send(string msg, string type)
        {
            for (int i = 0; i < noClients; i++)
            {
                clientServers[i].Send(msg, type);
            }
        }
        public string[] Receive()
        {
            string[] resps = new string[noClients];
            for (int i = 0; i < noClients; i++)
            {
                resps[i] = clientServers[i].Receive();
            }
            return resps;
        }
        private string RemoveEOF(string input)
        {
            if (input != "")
                return input.Remove(input.Length - 5, 5);
            else return input;
        }
    }
}
