﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace NettverkClient
{
    public class SocketClient
    {
        // Data buffer for data. 
        const int bufferLength = 1024;
        byte[] bytes = new byte[bufferLength];
        public string data = null;

        private Socket sender;
        private IPEndPoint remoteEP;

        private bool Sending = false;

        public string connectedIP;
        public bool IsSending()
        {
            return Sending;
        }

        private int ReceiveErrorCount = 0;

        public void RunClient(string ip, int port)
        {
            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
                IPAddress[] IPs = Dns.GetHostAddresses(ip);
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                //IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPAddress ipAddress = IPAddress.Parse(ip);
                remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP  socket.  
                sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                //the next step, connect it
                Connect();

                //then send hello packet
                //SendHello();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        private void SendHello()
        {
            string Hello = "Hello world!";
            Send(Hello, "00");
            Receive();
        }
        public string Receive()
        {
            try
            {
                // Receive the response from the remote device.  
                int bytesRec = 0;
                //needs to timeout every 6 secs if nothing is received
                sender.ReceiveTimeout = 6000;
                bytesRec = sender.Receive(bytes);
                
                //now get my bytesRec;
                if (bytesRec > 0)
                {
                    string str = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    return str;
                }
                //reset count
                ReceiveErrorCount = 0;
                return "";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                ReceiveErrorCount += 1;
                if (ReceiveErrorCount > 500)
                {
                    //kill thread
                }
                return "";
            }
        }
        private void Connect()
        {
            // Connect the socket to the remote endpoint. Catch any errors.  
            try
            {
                sender.Connect(remoteEP);

                Console.WriteLine("Socket connected to {0}",
                    sender.RemoteEndPoint.ToString());

                IPEndPoint remoteEnd = sender.RemoteEndPoint as IPEndPoint;
                connectedIP = remoteEnd.Address.ToString();
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }
        }
        public void Send(string message, string type)
        {
            Sending = true;
            try
            {
                // Encode the data string into a byte array.  
                byte[] msg = Encoding.ASCII.GetBytes(type+"<T>"+message + "<EOF>");

                // Send the data through the socket.  
                int bytesSent = sender.Send(msg);
            }

            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }

            Sending = false;
        }
        public void ShutdownClient()
        {
            // Release the socket.  
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }
}