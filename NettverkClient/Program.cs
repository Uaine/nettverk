﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace NettverkClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
            Console.WriteLine("What IP are we connecting to? Please no errors, I am too lazy to check for them.");
            string ip = Console.ReadLine();
            Console.WriteLine("Attempting to connect to: {0}", ip);
            NetHandler.RunClient(ip, 8113);

            while (true)
            {
                //send custom message
                Console.WriteLine("What is it you want to say?");
                string msg = Console.ReadLine();
                if (msg == "q")
                {
                    //quite program commence shutdown
                    NetHandler.Shutdown();
                    break;
                }
                else
                    NetHandler.Send(msg, "00");
            }
        }
    }
}