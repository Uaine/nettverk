﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace NettverkClient
{
    public static class NetHandler
    {
        static SocketClient myClient = new SocketClient();
        static Thread myClientReceiveThread = new Thread(new ThreadStart(ClientReceive));

        static bool Killing = false;
        static bool ThreadClosed = false;

        public static void RunClient(string ip, int port)
        {
            myClient.RunClient(ip, 8113);
            //now that we have connected and said hello let us get the receiving thread going
            myClientReceiveThread.Name = "myClientReceiveThread";
            myClientReceiveThread.IsBackground = true;
            myClientReceiveThread.Start();
        }
        public static void Shutdown()
        {
            //Need to disconnect from both ends and thus remove our receive thread
            Killing = true;
            //wait for thread to finish receiving
            while(true)
            {
                if (ThreadClosed == true)
                    break;
            }
            //reset variables for re-use
            ThreadClosed = false;
            myClient.ShutdownClient();
            Killing = false;
        }
        public static void Send(string msg, string type)
        {
            myClient.Send(msg, type);
        }
        static void ClientReceive()
        {
            while (true)
            {
                if (myClient.IsSending() == false)
                {
                    string msg = myClient.Receive();
                    if (msg != "")
                    {
                        string strype = msg.Remove(2, msg.Length - 2);
                        int type = Convert.ToInt32(strype);
                        msg = msg.Remove(0, 5);
                        HandleMessage(type, msg);
                    }
                }
                if (Killing == true)
                {
                    //terminate
                    ThreadClosed = true;
                    Thread.CurrentThread.Abort();
                }
            }
        }
        static void HandleMessage(int type, string message)
        {
            switch (type)
            {
                case 0:
                    Console.WriteLine(message);
                    break;
            }
        }
    }
}
