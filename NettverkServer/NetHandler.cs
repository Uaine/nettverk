﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace NettverkServer
{
    public static class NetHandler
    {
        static SocketServer myServer = new SocketServer();

        static bool Killing = false;
        static bool ThreadClosed = false;

        public static void RunServer()
        {
            myServer.RunServer("any", 8113);
            //now that we have a connection and it is launched
            Thread ReceiveThread = new Thread(new ThreadStart(ServerReceive));
            ReceiveThread.IsBackground = true;
            ReceiveThread.Name = "myServerRecThread";
            ReceiveThread.Start();
        }
        public static void Shutdown()
        {
            //Need to disconnect from both ends and thus remove our receive thread
            Killing = true;
            //wait for thread to finish receiving
            while (true)
            {
                if (ThreadClosed == true)
                    break;
            }
            //reset variables for re-use
            ThreadClosed = false;
            myServer.ShutdownServer();
            Killing = false;
        }
        public static void Send(string msg, string type)
        {
            myServer.Send(msg, type);
        }
        static private void ServerReceive()
        {
            while (true)
            {
                if (myServer.IsSending() == false)
                {
                    byte[] response = myServer.Receive();
                    string msg = Encoding.ASCII.GetString(response);
                    if (msg != "")
                    {
                        string strype = msg.Remove(2, msg.Length - 2);
                        int type = Convert.ToInt32(strype);
                        msg = msg.Remove(0, 5);
                        HandleMessage(type, msg);
                    }
                }
                if (Killing == true)
                {
                    //terminate
                    ThreadClosed = true;
                    Thread.CurrentThread.Abort();
                }
            }
        }
        static void HandleMessage(int type, string message)
        {
            switch (type)
            {
                case 0:
                    Console.WriteLine(message);
                    break;
            }
        }
    }
}