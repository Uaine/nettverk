﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace NettverkServer
{
    class Program
    {
        static void Main(string[] args)
        {
            NetHandler.RunServer();
            NetHandler.Send("test", "00");

            while(true)
            {
                Console.WriteLine("send a message?");
                string msg = Console.ReadLine();
                if (msg == "q")
                {
                    //quite program commence shutdown
                    NetHandler.Shutdown();
                    break;
                }
                else
                    NetHandler.Send(msg, "00");
            }
        }
    }
}
